package StepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefZero 
{
	
	/******************************************************/
	/*****************section for background***************/
	/******************************************************/
	
	@Given("^common given step$")
	public void common_given_step()
	{
		System.out.println("In common given");
	}
	
	@When("^common when step$")
	public void common_when_step()
	{
		System.out.println("In common when");
	}
	
	@Then("^common then step$")
	public void common_then_step()
	{
		System.out.println("In common then");
	}
	
	/******************************************************/
	/*****************section for hooks********************/
	/******************************************************/
	
	@Before
	public void beforeStep()
	{
		//System.out.println("In before step");
	}
	
	@After
	public void afterStep()
	{
		//System.out.println("In after step");
	}
	
	/******************************************************/
	/**************section for tagged hooks****************/
	/******************************************************/
	
	@Given("^given for first pre-step$")
	public void given_for_first_prestep()
	{
		System.out.println("given for first pre-step");
	}
	
	@When("^when for first pre-step$")
	public void when_for_first_prestep()
	{
		System.out.println("when for first pre-step");
	}
	
	@Then("^then for first pre-step$")
	public void then_for_first_prestep()
	{
		System.out.println("then for first pre-step");
	}
	
	@Given("^given for second pre-step$")
	public void given_for_second_prestep()
	{
		System.out.println("given for second pre-step");
	}
	
	@When("^when for second pre-step$")
	public void when_for_second_prestep()
	{
		System.out.println("when for second pre-step");
	}
	
	@Then("^then for second pre-step$")
	public void then_for_second_prestep()
	{
		System.out.println("then for second pre-step");
	}
}
