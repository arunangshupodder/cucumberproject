package StepDefinitions;

import java.util.List;
import java.util.Map;

import cucumber.api.DataTable;
import cucumber.api.java.en.When;

public class StepDefTwo 
{
	@When("^I enter text as \"(.*)\" and \"(.*)\"$")
	public void i_enter_text_as_parameters(String arg1, String arg2)
	{
		System.out.println("Entered text is: " + arg1 + " " + arg2);
	}
	
	@When("^I enter parameterized text$")
	public void i_enter_parameterized_text(DataTable dataTable)
	{
		List<List<String>> dataList = dataTable.raw();
		
		for (int i=0; i<dataList.size(); i++)
		{
			System.out.println("Entered text is: " + dataList.get(i).get(0) + " and " + dataList.get(i).get(1));
		}
		
	}
	
	@When("^I enter parameterized text again$")
	public void i_enter_parameterized_text_again(DataTable dataTable)
	{
		List<Map<String, String>> dataList = dataTable.asMaps(String.class, String.class);
		
		for (int i=0; i<dataList.size(); i++)
		{
			System.out.println("Entered text is: " + dataList.get(i).get("arg1") + " and " + dataList.get(i).get("arg2"));
		}
		
	}
}
