package StepDefinitions;

import java.util.List;
import java.util.Map;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class ApplicationLogin {
	
	@Given("^Application is loaded in browser$")
    public void application_is_loaded_in_browser() throws Throwable {
		//System.out.println("Application is loaded in browser");
       //throw new PendingException();
    }

    @When("^User clicks on login button$")//User clicks on login button
    public void user_clicks_on_login_button() throws Throwable {
    	//System.out.println("User clicks on login button");
        //throw new PendingException();
    }

    @Then("^User log into the application$")
    public void user_log_into_the_application() throws Throwable {
    	//System.out.println("User log into the application");
        //throw new PendingException();
    }

    @And("^User enters credentials \"(.*)\" and \"(.*)\"$")
    public void user_enters_credentials(String id, String password) throws Throwable {
    	//System.out.println("User enters credentials " + id + " " + password);
        //throw new PendingException();
    }

    @Then("^User clicks on login button with credentials from data table$")
    public void user_clicks_on_login_button_with_credentials_from_data_table(DataTable dataTable)
    {
    	List<List<String>> dataList = dataTable.raw();
    	
    	//System.out.println("User clicks on login button");
    	for (int i=0; i<dataList.size(); i++)
    	{
    		//System.out.println(dataList.get(i).get(0) + " " + dataList.get(i).get(1));
    	}
    }
    
    @Then("^User clicks on login button with credentials from maps in data table$")
    public void user_clicks_on_login_button_with_credentials_from_maps_in_data_table(DataTable dataTable)
    {
    	for(Map<String, String> dataMap : dataTable.asMaps(String.class, String.class))
    	{
    		//System.out.println(dataMap.get("UserId") + " " + dataMap.get("Password"));
    	}
    }
}
