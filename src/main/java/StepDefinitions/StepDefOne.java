package StepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class StepDefOne
{
	
	@Given("^I am learning Cucumber$")
	public void i_am_learning_cucumber()
	{
		System.out.println("I am learning cucumber");
	}
	
	@Given("^I am also learning Gherkin$")
	public void i_am_also_learning_gherkin()
	{
		System.out.println("I am also learning Gherkin");
	}
	
	@When("^I enter text$")
	public void i_enter_text()
	{
		System.out.println("I enter text");
	}
	
	@When("^I also enter parameters$")
	public void i__also_enter_parameters()
	{
		System.out.println("I also enter parameters");
	}
	
	@Then("^text should be displayed$")
	public void text_should_be_displayed()
	{
		System.out.println("Text should be displayed");
	}
	
	@Then("^error should not be displayed$")
	public void error_should_not_be_displayed()
	{
		System.out.println("Error should not be displayed");
	}
}