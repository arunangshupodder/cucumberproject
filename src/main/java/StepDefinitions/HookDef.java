package StepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;

public class HookDef 
{
	@Before("@First")
	public void beforeFirstStep()
	{
		//System.out.println("In before first step");
	}
	
	@After("@First")
	public void afterFirstStep()
	{
		//System.out.println("In after first step");
	}
	
	@Before("@Second")
	public void beforeSecondStep()
	{
		System.out.println("In before second step");
	}
	
	@After("@Second")
	public void afterSecondStep()
	{
		System.out.println("In after second step");
	}
}
