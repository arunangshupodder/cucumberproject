package GenericUtils;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/main/java/FeatureFiles/Login.feature",
		glue = {"StepDefinitions"},
		monochrome = true,
		plugin = {"pretty",
				  "html:target/cucumber-reports",
				  "json:target/cucumber-reports/Cucumber.json",
				  "junit:target/cucumber-reports/Cucumber.xml"}
		//format = { "pretty","html: cucumber-html-reports", "json: cucumber-html-reports/cucumber.json" }
		//tags = {"@UnitTest"}
		//"@UnitTest"
		//"@SmokeTest"
		//"@RegressionTest"
		//"@SmokeTest,@RegressionTest"		//example of or-ing 2 tags
		//"@SmokeTest", "@R	 egressionTest"	//example of and-ing 2 tags
		//"@SmokeTest", "~@RegressionTest"	//example of ignoring tags
		)
public class RunnerClass {
}
