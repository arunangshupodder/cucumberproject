Feature: Login feature of application

Scenario: Login to application
Given Application is loaded in browser
When User clicks on login button
And User enters credentials "admin" and "password"
Then User log into the application

Scenario Outline: User logs in to application using credentials
Given Application is loaded in browser
When User clicks on login button
And User enters credentials "<userId>" and "<password>"
Then User log into the application
Examples:
|userId	|password	|
|admin1	|password1	|
|admin2	|password2	|

Scenario: Login to application
Given Application is loaded in browser
When User clicks on login button with credentials from data table
|admin3	|password3	|
|admin4	|password4	|
Then User log into the application

Scenario: Login to application
Given Application is loaded in browser
When User clicks on login button with credentials from maps in data table
|UserId	|Password	|
|admin5	|password7	|
|admin6	|password8	|
Then User log into the application