$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("FeatureFile.feature");
formatter.feature({
  "line": 1,
  "name": "Demo",
  "description": "",
  "id": "demo",
  "keyword": "Feature"
});
formatter.before({
  "duration": 746689,
  "status": "passed"
});
formatter.background({
  "line": 4,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 5,
  "name": "common given step",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "common when step",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "common then step",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef.common_given_step()"
});
formatter.result({
  "duration": 490440168,
  "status": "passed"
});
formatter.match({
  "location": "StepDef.common_when_step()"
});
formatter.result({
  "duration": 262934,
  "status": "passed"
});
formatter.match({
  "location": "StepDef.common_then_step()"
});
formatter.result({
  "duration": 286690,
  "status": "passed"
});
formatter.scenario({
  "comments": [
    {
      "line": 49,
      "value": "#Example of data table"
    }
  ],
  "line": 51,
  "name": "",
  "description": "",
  "id": "demo;",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 50,
      "name": "@datatable"
    }
  ]
});
formatter.step({
  "line": 52,
  "name": "I am learning Cucumber",
  "keyword": "Given "
});
formatter.step({
  "line": 53,
  "name": "I enter text from data table",
  "rows": [
    {
      "cells": [
        "data",
        "value"
      ],
      "line": 54
    },
    {
      "cells": [
        "data1",
        "value1"
      ],
      "line": 55
    },
    {
      "cells": [
        "data2",
        "value2"
      ],
      "line": 56
    },
    {
      "cells": [
        "data3",
        "value3"
      ],
      "line": 57
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 58,
  "name": "text should be displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "StepDef.i_am_learning_cucumber()"
});
formatter.result({
  "duration": 231080,
  "status": "passed"
});
formatter.match({
  "location": "StepDef.text_from_data_table(DataTable)"
});
formatter.result({
  "duration": 5541047,
  "status": "passed"
});
formatter.match({
  "location": "StepDef.text_should_be_displayed()"
});
formatter.result({
  "duration": 248357,
  "status": "passed"
});
formatter.after({
  "duration": 238638,
  "status": "passed"
});
});