Feature: Demo

#Background: pre-step for every scenario
#Given common given step
#When common when step
#Then common then step

@First
Scenario: first pre-step
Given given for first pre-step
When when for first pre-step
Then then for first pre-step

@Second
Scenario: second pre-step
Given given for second pre-step
When when for second pre-step
Then then for second pre-step

@UnitTest
Scenario: simple scenario
Given I am learning Cucumber
When I enter text
Then text should be displayed
But error should not be displayed

#@UnitTest
#Scenario: use of the star keyword
#* I am learning Cucumber
#* I enter text
#* text should be displayed

@SmokeTest
Scenario: scenario with and
Given I am learning Cucumber
And I am also learning Gherkin
When I enter text
And I also enter parameters
Then text should be displayed

@RegressionTest
Scenario: parameterizing a scenario
Given I am learning Cucumber
When I enter text as "Hello" and "World"
Then text should be displayed

#this scenario will run twice. Hence it will be considered as 2 different scenarios in the report
@SmokeTest @RegressionTest
Scenario Outline: parameterizing a scenario with examples
Given I am learning Cucumber
When I enter text as "<userid>" and "<password>"
Then text should be displayed
Examples:
	|	userid		|	password	|
	|	apodder1	|	pwd1		|
	|	apodder2	|	pwd2		|

@SmokeTest
Scenario: parameterizing scenario with data tables
Given I am learning Cucumber
When I enter parameterized text
	|	text1	|	text2	|
	|	text3	|	text4	|
	|	text5	|	text6	|
Then text should be displayed

@RegressionTest
Scenario: parameterizing scenario with maps in data tables
Given I am learning Cucumber
When I enter parameterized text again
	|	arg1	|	arg2	|
	|	text7	|	text8	|
	|	text9	|	text10	|
	|	text11	|	text12	|
Then text should be displayed