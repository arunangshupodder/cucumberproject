$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/main/java/FeatureFiles/Login.feature");
formatter.feature({
  "line": 1,
  "name": "Login feature of application",
  "description": "",
  "id": "login-feature-of-application",
  "keyword": "Feature"
});
formatter.before({
  "duration": 462093,
  "status": "passed"
});
formatter.scenario({
  "line": 3,
  "name": "Login to application",
  "description": "",
  "id": "login-feature-of-application;login-to-application",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "Application is loaded in browser",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "User clicks on login button",
  "keyword": "When "
});
formatter.step({
  "line": 6,
  "name": "User enters credentials \"admin\" and \"password\"",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "User log into the application",
  "keyword": "Then "
});
formatter.match({
  "location": "ApplicationLogin.application_is_loaded_in_browser()"
});
formatter.result({
  "duration": 457433732,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationLogin.user_clicks_on_login_button()"
});
formatter.result({
  "duration": 72877,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin",
      "offset": 25
    },
    {
      "val": "password",
      "offset": 37
    }
  ],
  "location": "ApplicationLogin.user_enters_credentials(String,String)"
});
formatter.result({
  "duration": 8456949,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationLogin.user_log_into_the_application()"
});
formatter.result({
  "duration": 51284,
  "status": "passed"
});
formatter.after({
  "duration": 37788,
  "status": "passed"
});
formatter.scenarioOutline({
  "line": 9,
  "name": "User logs in to application using credentials",
  "description": "",
  "id": "login-feature-of-application;user-logs-in-to-application-using-credentials",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 10,
  "name": "Application is loaded in browser",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "User clicks on login button",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "User enters credentials \"\u003cuserId\u003e\" and \"\u003cpassword\u003e\"",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "User log into the application",
  "keyword": "Then "
});
formatter.examples({
  "line": 14,
  "name": "",
  "description": "",
  "id": "login-feature-of-application;user-logs-in-to-application-using-credentials;",
  "rows": [
    {
      "cells": [
        "userId",
        "password"
      ],
      "line": 15,
      "id": "login-feature-of-application;user-logs-in-to-application-using-credentials;;1"
    },
    {
      "cells": [
        "admin1",
        "password1"
      ],
      "line": 16,
      "id": "login-feature-of-application;user-logs-in-to-application-using-credentials;;2"
    },
    {
      "cells": [
        "admin2",
        "password2"
      ],
      "line": 17,
      "id": "login-feature-of-application;user-logs-in-to-application-using-credentials;;3"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 84753,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "User logs in to application using credentials",
  "description": "",
  "id": "login-feature-of-application;user-logs-in-to-application-using-credentials;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 10,
  "name": "Application is loaded in browser",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "User clicks on login button",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "User enters credentials \"admin1\" and \"password1\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "User log into the application",
  "keyword": "Then "
});
formatter.match({
  "location": "ApplicationLogin.application_is_loaded_in_browser()"
});
formatter.result({
  "duration": 109045,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationLogin.user_clicks_on_login_button()"
});
formatter.result({
  "duration": 63160,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin1",
      "offset": 25
    },
    {
      "val": "password1",
      "offset": 38
    }
  ],
  "location": "ApplicationLogin.user_enters_credentials(String,String)"
});
formatter.result({
  "duration": 323897,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationLogin.user_log_into_the_application()"
});
formatter.result({
  "duration": 59921,
  "status": "passed"
});
formatter.after({
  "duration": 41567,
  "status": "passed"
});
formatter.before({
  "duration": 114443,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "User logs in to application using credentials",
  "description": "",
  "id": "login-feature-of-application;user-logs-in-to-application-using-credentials;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 10,
  "name": "Application is loaded in browser",
  "keyword": "Given "
});
formatter.step({
  "line": 11,
  "name": "User clicks on login button",
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "User enters credentials \"admin2\" and \"password2\"",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "User log into the application",
  "keyword": "Then "
});
formatter.match({
  "location": "ApplicationLogin.application_is_loaded_in_browser()"
});
formatter.result({
  "duration": 141435,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationLogin.user_clicks_on_login_button()"
});
formatter.result({
  "duration": 87452,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "admin2",
      "offset": 25
    },
    {
      "val": "password2",
      "offset": 38
    }
  ],
  "location": "ApplicationLogin.user_enters_credentials(String,String)"
});
formatter.result({
  "duration": 361145,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationLogin.user_log_into_the_application()"
});
formatter.result({
  "duration": 67479,
  "status": "passed"
});
formatter.after({
  "duration": 47505,
  "status": "passed"
});
formatter.before({
  "duration": 135497,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Login to application",
  "description": "",
  "id": "login-feature-of-application;login-to-application",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 20,
  "name": "Application is loaded in browser",
  "keyword": "Given "
});
formatter.step({
  "line": 21,
  "name": "User clicks on login button with credentials from data table",
  "rows": [
    {
      "cells": [
        "admin3",
        "password3"
      ],
      "line": 22
    },
    {
      "cells": [
        "admin4",
        "password4"
      ],
      "line": 23
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 24,
  "name": "User log into the application",
  "keyword": "Then "
});
formatter.match({
  "location": "ApplicationLogin.application_is_loaded_in_browser()"
});
formatter.result({
  "duration": 174364,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationLogin.user_clicks_on_login_button_with_credentials_from_data_table(DataTable)"
});
formatter.result({
  "duration": 6435833,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationLogin.user_log_into_the_application()"
});
formatter.result({
  "duration": 60461,
  "status": "passed"
});
formatter.after({
  "duration": 47505,
  "status": "passed"
});
formatter.before({
  "duration": 93930,
  "status": "passed"
});
formatter.scenario({
  "line": 26,
  "name": "Login to application",
  "description": "",
  "id": "login-feature-of-application;login-to-application",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 27,
  "name": "Application is loaded in browser",
  "keyword": "Given "
});
formatter.step({
  "line": 28,
  "name": "User clicks on login button with credentials from maps in data table",
  "rows": [
    {
      "cells": [
        "UserId",
        "Password"
      ],
      "line": 29
    },
    {
      "cells": [
        "admin5",
        "password7"
      ],
      "line": 30
    },
    {
      "cells": [
        "admin6",
        "password8"
      ],
      "line": 31
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 32,
  "name": "User log into the application",
  "keyword": "Then "
});
formatter.match({
  "location": "ApplicationLogin.application_is_loaded_in_browser()"
});
formatter.result({
  "duration": 138736,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationLogin.user_clicks_on_login_button_with_credentials_from_maps_in_data_table(DataTable)"
});
formatter.result({
  "duration": 1065621,
  "status": "passed"
});
formatter.match({
  "location": "ApplicationLogin.user_log_into_the_application()"
});
formatter.result({
  "duration": 64239,
  "status": "passed"
});
formatter.after({
  "duration": 45885,
  "status": "passed"
});
});